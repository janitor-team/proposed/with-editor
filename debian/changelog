with-editor (3.0.5-1) unstable; urgency=medium

  * New upstream release.

 -- Rémi Vanicat <vanicat@debian.org>  Sun, 17 Oct 2021 18:24:28 +0200

with-editor (3.0.4-1) unstable; urgency=medium

  * Team upload

  [ Rémi Vanicat ]
  * New upstream release.
  * Update the watch file

  [ Matteo F. Vescovi ]
  * debian/watch: version bump 3 -> 4
  * debian/control: S-V bump 4.1.4 -> 4.6.0 (no changes needed)

 -- Rémi Vanicat <vanicat@debian.org>  Fri, 20 Aug 2021 09:09:04 +0200

with-editor (3.0.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 07 Nov 2020 12:26:22 -0700

with-editor (2.9.1-2) unstable; urgency=medium

  [ Nicholas D Steeves]
  * Team upload.
  * Drop emacs25 from Enhances; it has been a dummy package since buster.
  * Switch to debhelper-compat 13.
  * control: Add Rules-Requires-Root: no.

  [ Sven Joachim ]
  * d/rules: Compute VERSION from d/changelog rather than hardcoding it,
    which means it gets outdated quickly (Closes: #924512)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Sun, 31 May 2020 22:56:08 -0400

with-editor (2.9.1-1) unstable; urgency=medium

  [ David Krauser ]
  * Update maintainer email address

  [ Rémi Vanicat ]

  * New upstream version

 -- Rémi Vanicat <vanicat@debian.org>  Mon, 13 Jan 2020 17:27:52 +0100

with-editor (2.8.3-1) unstable; urgency=medium

  * New upstream version

 -- Rémi Vanicat <vanicat@debian.org>  Mon, 29 Jul 2019 11:17:46 +0200

with-editor (2.8.1-1) unstable; urgency=medium

  * New upstream version

 -- Rémi Vanicat <vanicat@debian.org>  Wed, 02 Jan 2019 15:12:14 +0100

with-editor (2.8.0-1) unstable; urgency=medium

  * New upstream version

 -- Rémi Vanicat <vanicat@debian.org>  Sun, 11 Nov 2018 14:05:02 +0100

with-editor (2.7.4-1) unstable; urgency=medium

  * New upstream version
  * Update version in debhelper depends
  * Remove unnecessary dh argument --parallel

 -- Rémi Vanicat <vanicat@debian.org>  Sun, 29 Jul 2018 11:47:15 +0200

with-editor (2.7.3-1) unstable; urgency=medium

  * New upstream version
  * Change debhelper compat level
  * Use secure URLs
  * Lowercase Description
  * Update standards version to 4.1.4 (no changes)
  * Drop emacs24 from Enhances
  * Switch Vcs from alioth to salsa

 -- Rémi Vanicat <vanicat@debian.org>  Sat, 09 Jun 2018 18:43:43 +0200

with-editor (2.7.1-1) unstable; urgency=medium

  * New upstream version
  * Update standards version to 4.1.3 (no changes)
  * Add autoload

 -- Rémi Vanicat <vanicat@debian.org>  Tue, 13 Feb 2018 21:48:38 +0100

with-editor (2.6.0-1) unstable; urgency=medium

  * New upstream version
  * Drop 0002-Do-not-depend-on-async.patch
  * Drop build dependency on dash

 -- Rémi Vanicat <vanicat@debian.org>  Wed, 13 Sep 2017 08:47:30 +0200

with-editor (2.5.11-1) unstable; urgency=medium

  * New upstream version.
  * Update depend for new dh-elpa.
  * Drop 0001-Do-not-depend-on-elpa-dash-package.patch.
  * Upgrades standard version to 4.0.0 (no change).

 -- Rémi Vanicat <vanicat@debian.org>  Mon, 19 Jun 2017 17:52:25 +0200

with-editor (2.5.9-1) unstable; urgency=medium

  * New upstream version.

 -- Rémi Vanicat <vanicat@debian.org>  Sat, 07 Jan 2017 13:34:53 +0100

with-editor (2.5.8-1) unstable; urgency=medium

  * New upstream version.

 -- Rémi Vanicat <vanicat@debian.org>  Fri, 09 Dec 2016 18:54:55 +0100

with-editor (2.5.7-1) unstable; urgency=medium

  * New upstream version.

 -- Rémi Vanicat <vanicat@debian.org>  Sun, 30 Oct 2016 18:54:08 +0100

with-editor (2.5.6-1) unstable; urgency=medium

  * New upstream version.

 -- Rémi Vanicat <vanicat@debian.org>  Fri, 07 Oct 2016 20:20:19 +0200

with-editor (2.5.5-1) unstable; urgency=medium

  * New upstream version.

 -- Rémi Vanicat <vanicat@debian.org>  Fri, 30 Sep 2016 09:18:53 +0200

with-editor (2.5.3-1) unstable; urgency=medium

  * New upstream version.

 -- Rémi Vanicat <vanicat@debian.org>  Fri, 16 Sep 2016 11:02:29 +0200

with-editor (2.5.1-1) unstable; urgency=low

  * New upstream version.
  * Upgrade to standard version to 3.9.8 (no change).

 -- Rémi Vanicat <vanicat@debian.org>  Fri, 06 May 2016 17:15:33 +0200

with-editor (2.5.0-1) unstable; urgency=medium

  * As upstream has split this from magit, creating a new package.
  * Closing ITP (Closes: #813286).

 -- Rémi Vanicat <vanicat@debian.org>  Sun, 31 Jan 2016 11:37:51 +0100
